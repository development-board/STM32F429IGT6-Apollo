# 移植过程中，修改步骤：

## 修改Makefile

### 添加 context_gcc.s(context_gcc.S文件修改过来，将S改成s)到工程

![1](pic/1.png)

### 将硬浮点改成软浮点

![2](pic/2.png)

### 添加相关命令

![3](pic/3.png)

```makefile
load: all
  STM32_Programmer_CLI  -c port=SWD  mode=NORMAL -d build/$(TARGET).bin 0x08000000 -s
reset:
  STM32_Programmer_CLI  -c port=SWD  mode=NORMAL -rst
erase:
  STM32_Programmer_CLI  -c port=SWD  mode=NORMAL -e all
clean:
  del /q $(BUILD_DIR)
```

##  修改链接文件（STM32F429IGTx_FLASH.ld）

![4](pic/4.png)

## 修改启动文件（start_stm323f429xx.s）


![5](pic/5.png)

## main函数的while循环中加延时函数

![6](pic/6.png)



**注意： 不要使用env环境进行代码编译，可能是交叉编译器的差别可能会导致代码不运行**